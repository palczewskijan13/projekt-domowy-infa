#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include "winbgi2.h"
int n=49,r=11,fps=60;	//parametry, glownie do testow- ilosc pilek, promien ,ilosc klatek na sekunde
double spd=0.5;	//predkosc wyswietlania
double strt=0.9,moc=500000./fps;				//straty(ile energii w poszczegolnym kierunku sie zachowa),moc dmuchawy
double x[49],y[49],vx[49],vy[49];
int xo=700,yo=500, blok=200;		//wspolrzedne krancowe pudelka i dol blokady
int xd[5],yd[5];		//tablice na wspolrzedne dmuchaw
double g=98.1/fps;
double vjx,vjy,vnx,vny,nx,ny;		
double v,vnew;						//zmienne pomocnicze do obliczen fizyki
double odl,odly,odlx;
int zlap[6]={-2,-2,-2,-2,-2,-2};			//tablica na zlapane kulki
void puddmuch()				//funkcja rysujca pudelko (i dmuchawe do testow)
{
	line(100,100,xo,100);
	line(xo,100,xo,yo);
	line(xo,yo,100,yo);
	line(100,yo,100,100);
	/*for(int i=0;i<5;i++)
	{
		line(xd[i],yd[i],xd[i]+100,yd[i]-50);
		line(xd[i],yd[i],xd[i]-100,yd[i]-50); //kod do rysowania dmuchaw
	}
	*/
}
void energy(int i)		//funkcja zmniejszajaca energie kinetyczna, na po zderzeniu
{
	v=sqrt((pow(vx[i],2)+pow(vy[i],2)));
	vnew=sqrt(strt)*v;		//e=mv^2/2, wiec vnew^2=strt*v^2=>	vnew=sqrt(strt)*v
	vx[i]=vnew*(vx[i]/v);
	vy[i]=vnew*(vy[i]/v);
}	
void init()				//funkcja inicjalizuj�ca pi�ki
{
	puddmuch();
	
	for(int i=0;i<n;i++)
		{	
			vx[i]=0;
			vy[i]=0;
			x[i]=rand()/(double)RAND_MAX*(xo-r-100-r)+100+r;			
			y[i]=rand()/(double)RAND_MAX*(blok-r-100-r)+100+r;
			for(int j=0;j<i;j++)
			{
				odl=(double)sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j]));		//jesli wylosowane kulki sa w sobie, losuj jeszcze raz
				if(odl<(2*r))
				{
					i--;
					break;
				}
			}
		}
	
	
}
void gravity()
{
	for(int i=0;i<n;i++)
	{
		vy[i]+=g;
	}
}

void dmuchawa()		//im blizej do dmuchawy, tym bardziej bedzie przyspieszac
{
	for(int i=0;i<5;i++)
	{	
		xd[i]=rand()/(double)RAND_MAX*(xo-100)+100;
		yd[i]=rand()/(double)RAND_MAX*(yo-100)+100+r; //losowe ustawienie wysokosci dmuchawy
	}
	for(int j=0;j<5;j++)
	{
		for(int i=0;i<n;i++)
		{
			odlx=x[i]-xd[j];
			odly=y[i]-yd[j];
			odl=sqrt(pow(odlx,2)+pow(odly,2));
			//printf("%lf\t%lf\t%lf\t%lf\n",odlx,odly,odl,vx[i]);		//test dmuchawy
			vy[i]+=moc/pow(odl,2)*(odly/odl);
			vx[i]+=moc/pow(odl,2)*(odlx/odl);
			//printf("j=%d %lf\t%lf\t",j,moc/pow(odl,2),odl);
		}
		//printf("\t");
	}
}
double konlew,kongor;	//zmienne potrzebne do lapania
void sciany(double dol)
{ 
	for(int i=0;i<n;i++)
	{
		if((x[i]+r)>xo)
		{
			vx[i]=(-vx[i]);
			x[i]=(xo-r-1);
			energy(i);
		}

		else if ((x[i]-r)<100)
		{
			vx[i]=(-vx[i]);
			x[i]=(100+r+1);
			energy(i);
		}
		if((y[i]+r)>dol)
		{
			vy[i]=(-vy[i]);
			y[i]=(dol-r-1);
			energy(i);
		}
		else if((y[i]-r)<100)
		{
			vy[i]=(-vy[i]);
			y[i]=(100+r+1);
			energy(i);
		}
	}
}

void run()
{
	for(int i=0;i<n;i++)
	{
		x[i] += spd*vx[i]/fps; 
		y[i] += spd*vy[i]/fps;
	}
}
void zderzenia()
{	
	
	for(int i=1;i<n;i++)
	{
		for(int j=0;j<i;j++)
		{
			odl=(double)sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j]));
			if(odl<(2*r))
			{
				vjx=(vx[i]-vx[j]);
				vjy=(vy[i]-vy[j]);
					//printf("%vxi=%lf\tvyi=%lf\n%vxj=%lf\tvyj=%lf\nvjx=%lf\tvjy=%lf\n",vx[i],vy[i],vx[j],vy[j],vjx,vjy);
				nx=(x[i]-x[j])/(odl);
				x[i]+=nx*2*r/odl;	//zmiana miejsca, zeby nie wygl�dalo jakby na siebie nachodzilo
				ny=(y[i]-y[j])/(odl);			
				y[i]+=ny*2*r/odl;
					//printf("nx=%lf\tny=%lf\n",nx,ny);
				vnx=(vjx*nx+vjy*ny)*nx;
					//printf("vnx=%lf\t",vnx);
				vny=(vjx*nx+vjy*ny)*ny;
					//printf("vny=%lf\n",vny);
				vx[i]+=(-vnx);
				vy[i]+=(-vny);
					//printf("Po fakcie \nvxi=%lf\tvyi=%lf\n",vx[i],vy[i]);
				vx[j]+=(vnx);
				vy[j]+=(vny);
					//printf("Po fakcie \nvxj=%lf\tvyj=%lf\n\n\n",vx[j],vy[j]);

				energy(i);
				energy(j);
					//getchar();
			}
		}
	}
}
void rysuj()
{	
	char nr[3];
	for(int i=0;i<n;i++)
	{
		_itoa_s(i+1,nr,10);
		outtextxy(x[i]-(r/2),y[i]-(r/2),nr);
		circle(x[i],y[i],r);
		//printf("i=%d x=%lf\ty=%lf\n",i+1,x[i],y[i]);
	}

}
int zlicz=0;	//zmienna zliczajaca ile  liczb jest z�apanych
int wciaganie()
{

	konlew=xo;
	line(xo,kongor,xo,yo);
	for(int j=0;j<6;j++)
	{
		//line(xo,kongor,xo,yo);
		line(konlew,yo-2*r,konlew,yo);
		
		if(zlap[j]<-1)		//jesli miejsce kt�re sprawdzamy jest puste
		{
			for(int i=0;i<n;i++)
			{	
				if((x[i] >= (konlew-(2*r)))&(x[i] <= (konlew))&(y[i] >= kongor))
				{
					printf("trafione %d\t miejsce %d\n",(i+1),6-j);
					vx[i]=0;
					vy[i]=0;
					zlap[j]=i;
					x[i]=konlew-r;
					y[i]=kongor+r;
					vx[zlap[j]]=0;
					vy[zlap[j]]=0;
					zlicz++;
				
				}
			}
		}

		  if(zlap[j] > -1)
		{
			
			for(int i=0;i<n;i++)
			{
				if((x[i] >= (konlew-(2*r)))&(x[i] <= (konlew))&(y[i] >= kongor))
				{
					vy[i]=(-vy[i]);
					y[i]=(kongor-r-1);
					energy(i);
				}
			vx[zlap[j]]=0;
			vy[zlap[j]]=0;
			x[zlap[j]]=konlew-r;
			y[zlap[j]]=kongor+r;
			}
			
			//getchar();
		}
			
			konlew-=2*r;
	}
	
	line(konlew,yo-2*r-1,konlew,yo);
	line(xo,kongor,konlew,kongor);
	return zlicz;

}
int main()
{	
	srand(time(NULL));
	graphics(800,600);
	init();
	rysuj();
	//getchar();
	while(animate(fps))		//pi�ki zablokowane
	{ 
		sciany(blok);
		gravity();
		zderzenia();
		run();
		cleardevice();
		line(100,blok,xo,blok);
		puddmuch();
		rysuj();

		if(whichmousebutton()!=0) break;
	}
	clearmouse();
	while(animate(fps))		//pi�ki zwolnione
	{ 
		sciany(yo);
		zderzenia();
		gravity();
		dmuchawa();
		run();
		cleardevice();
		puddmuch();
		rysuj();
		if(whichmousebutton()!=1) break;
	}
	kongor=yo-2*r-1;
	while(animate(fps))			//�apanie pi�ek
	{ 
		sciany(yo);
		zderzenia();
		gravity();
		dmuchawa();
		cleardevice();
		puddmuch();
		if(wciaganie()==6)
		{
			run();
			rysuj();
			break;
		}
		run();
		rysuj();
		//printf("%d\n",wciaganie());
	}
	printf("\n Wylosowane liczby to:\n") ;
	for(int i=5;i>=0;i--)
	{
		printf("%d\t",zlap[i]+1);
	}
	wait();
	return 0;
}